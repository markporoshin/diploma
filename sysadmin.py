from user import User
import datetime
import random
from math import fabs


class SysAdmin(User):

    def __init__(self, host, user, psw):
        super().__init__(host, user, psw)

        self.actions = [
            {
                'exe': self.insert_tests_db,
                'prob': 1,
                'name': 'insert_tests_db'
            },
            {
                'exe': self.select_tests_db,
                'prob': 1,
                'name': 'select_tests_db'
            },
            # {
            #     'exe': self.del_user,
            #     'prob': 1,
            #     'name': 'del_user'
            # },
            # {
            #     'exe': self.add_user,
            #     'prob': 1,
            #     'name': 'add_user'
            # },
            {
                'exe': self.create_file,
                'prob': 1,
                'name': 'create_file'
            },
            {
                'exe': self.del_file,
                'prob': 1,
                'name': 'del_file'
            },
            {
                'exe': self.ping_google,
                'prob': 1,
                'name': 'ping_google'
            },
            {
                'exe': self.create_dir,
                'prob': 1,
                'name': 'create_dir'
            },
            {
                'exe': self.del_dir,
                'prob': 1,
                'name': 'del_dir'
            },
            {
                'exe': self.req_to_ngnix,
                'prob': 1,
                'name': 'req_to_ngnix'
            },
            {
                'exe': self.req_to_tomcat,
                'prob': 1,
                'name': 'req_to_tomcat'
            }
        ]
        self.get_series = lambda: int(fabs(random.gauss(1, 5)))
        self.freq = 1
        self._set_logger('resource/temp/SysAdmin.log')
        self.created_users = []
        self.created_files = []
        self.created_dirs = []
        self.file_limit = 20
        self.dir_limit = 20
        self.user_limit = 20

    def insert_tests_db(self):
        query = f"insert into test (info) values ('{self.rw.generate()}')"
        command = f'sudo -u postgres psql -d diploma -c "{query}"'
        anws = self.ssh.execute(command)
        self.logger.info(command + "\t||\t" + str(anws))

    def select_tests_db(self):
        query = "select * from test"
        command = f'sudo -u postgres psql -d diploma -c "{query}"'
        anws = self.ssh.execute(command)
        self.ssh.execute('q')
        self.logger.info(command + "\t||\t" + str(anws))

    def del_user(self):
        if not self.created_users:
            return
        username = random.choice(self.created_users)
        command = f"sudo userdel {username}"
        self.created_users.remove(username)
        anws = self.ssh.execute(command)
        self.logger.info(command + "\t||\t" + str(anws))

    def add_user(self):
        self.logger.info(f"created_users: {str(self.created_users)}")
        if len(self.created_users) > self.user_limit:
            return
        user = self.rw.generate()
        command = f"sudo adduser  --force-badname {user.lower()}"
        anws = self.ssh.execute(command)
        self.created_users.append(user)
        self.logger.info(command + "\t||\t" + str(anws))

    def ping_google(self):
        command = f"ping -c 5 www.google.com"
        answ = self.ssh.execute(command)
        self.logger.info(command + "\t||\t" + str(answ))

    def del_file(self):
        if not self.created_files:
            return
        file = random.choice(self.created_files)
        command = f"rm {file}"
        answ = self.ssh.execute(command)
        self.created_files.remove(file)
        self.logger.info(command + "\t\t" + str(answ))

    def create_file(self):
        self.logger.info(f"created_files: {str(self.created_files)}")
        if len(self.created_files) > self.file_limit:
            return
        start_path = '/home/face/Documents/Workspace/'
        path = self.rand_dir(start_path)
        if '/home/face/Documents/Workspace/' not in path:
            self.logger.info("cannot choose dir")
            return
        file = path + self.rw.generate()
        command = f'touch {file}.txt'
        self.created_files.append(file)
        answ = self.ssh.execute(command)
        self.logger.info(command + "\t\t" + str(answ))
        command = f"echo '#new commend by sysadmin {datetime.datetime.today().strftime('%Y-%m-%d-%H:%M:%S')}' >> {file}"
        answ = self.ssh.execute(command)
        self.logger.info(command + "\t||\t" + str(answ))

    def create_dir(self):
        self.logger.info(f"created_dirs: {str(self.created_files)}")
        if len(self.created_dirs) > self.dir_limit:
            return
        start_path = '/home/face/Documents/Workspace/'
        path = self.rand_dir(start_path)
        file = path + self.rw.generate()
        command = f'mkdir {file}'
        self.created_dirs.append(file)
        answ = self.ssh.execute(command)
        self.logger.info(command + "\t||\t" + str(answ))

    def del_dir(self):
        if not self.created_dirs:
            return
        dir = random.choice(self.created_dirs)
        command = f"rm -r {dir}"
        answ = self.ssh.execute(command)
        self.created_dirs.remove(dir)
        self.logger.info(command + "\t||\t" + str(answ))

    def rand_dir(self, path):
        for _ in range(random.randint(0, 2)):
            command = f'ls -d {path}*/ | shuf -n1'
            new_path = (self.ssh.execute(command) + [path])[0]
            if 'ls: cannot access' in new_path:
                return path
            path = new_path
            self.logger.info(command + "\t||\t" + path)
        return path

    def req_to_ngnix(self):
        command = f'curl 10.0.0.17'
        answ = self.ssh.execute(command)
        self.logger.info(command + "\t||\t" + str(answ))

    def req_to_tomcat(self):
        command = f'curl 10.0.0.17:8080'
        answ = self.ssh.execute(command)
        self.logger.info(command + "\t||\t" + str(answ))


if __name__ == '__main__':
    sysadmin = SysAdmin('10.0.0.17', 'face', 'gucci')
    try:
        random.seed(1)
        sysadmin.run()
    except Exception as e:
        sysadmin.logger.error(str(e))
