import paramiko
import socket
import time


class Ssh:

    max_bytes = 1024

    def __init__(self, host, user, psw, sleep=0.5):
        self.client = paramiko.SSHClient()
        self.psw = psw
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(hostname=host, username=user, password=psw, port=22)
        self.shell = self.client.invoke_shell()
        self.shell.settimeout(1)
        output = ""
        while True:
            try:
                part = self.shell.recv(Ssh.max_bytes).decode("utf-8")
                output += part
            except socket.timeout:
                break

    def execute(self, command, sleep=0.5):
        self.shell.send(f"{command}\n")
        output = ""
        while True:
            try:
                part = self.shell.recv(Ssh.max_bytes).decode("utf-8")
                output += part
                # time.sleep(sleep)
            except socket.timeout:
                break
        if 'password' in output:
            return output.split('\r\n')[1:-1] + self.execute(self.psw)
        return output.split('\r\n')[1:-1]

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.shell.close()
        self.client.close()


if __name__ == "__main__":
    with Ssh('10.0.0.17', 'face', 'gucci') as ssh:
        print(ssh.execute("ls /home/"))
        print(ssh.execute("ls /home/"))
