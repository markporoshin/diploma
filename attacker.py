from user import User
import random


class Attacker(User):

    def __init__(self, host, user, psw):
        super().__init__(host, user, psw)
        self.actions = [self.copy_many_files, self.download_many_files]
        self.actions_probs = [100, 1]
        self.actions_names = ['copy_many_files', 'download_many_files']

        self.actions = [
            {
                'exe': self.copy_many_files,
                'prob': 1,
                'name': 'copy_many_files'
            },
            {
                'exe': self.download_many_files,
                'prob': 1,
                'name': 'download_many_files'
            }
        ]

        self.freq = 1 / (60 * 10)  # event per 10 hour
        self._set_logger('resource/temp/Attacker.log')

    def copy_many_files(self):
        number_of_execution = random.randint(50, 100)
        self.logger.info(f'copy {number_of_execution} files')
        for _ in range(number_of_execution):
            src = 'face@10.0.0.19:/home/face/Documents/dont_touch.txt'
            dst = f'/home/face/Documents/Workspace/__{self.rw.generate()}.scp'
            command = f"scp {src} {dst}"
            anws = self.ssh.execute(command)
            self.logger.info(command + "\t||\t" + str(anws))

    def download_many_files(self):
        number_of_execution = random.randint(50, 100)
        self.logger.info(f'download {number_of_execution} files')
        for _ in range(number_of_execution):
            command = f"wget -O /home/face/Documents/Workspace/{self.rw.generate()}.wget www.examplesite.com/textfile.txt"
            anws = self.ssh.execute(command)
            self.logger.info(command + "\t||\t" + str(anws))


if __name__ == '__main__':
    attacker = Attacker('10.0.0.17', 'face', 'gucci')
    attacker.run()