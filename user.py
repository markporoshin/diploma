from RandomWordGenerator import RandomWord
from ssh import Ssh
from math import fabs
import numpy as np
from time import sleep
import random
import logging


class User:

    def __init__(self, host, user, psw):
        self.ssh = Ssh(host, user, psw)
        self.logger = None
        self.psw = psw
        self.user = user
        self.host = host
        self.actions = []
        self.actions_probs = []
        self.actions_names = []
        self.freq = 0
        self.get_series = lambda : 1
        self.rw = RandomWord(10,
                constant_word_size=True,
                include_digits=False,
                special_chars=r"@_!#$%^&*()<>?/\|}{~:",
                include_special_chars=False)

    def _set_logger(self, file_name):
        logger_handler = logging.FileHandler(file_name)
        logger_formatter = logging.Formatter('%(asctime)s - %(levelname)s -\t\t %(message)s')
        logger_handler.setFormatter(logger_formatter)
        self.logger = logging.getLogger(__name__)
        logging.getLogger().setLevel(logging.INFO)
        self.logger.addHandler(logger_handler)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.ssh.__exit__(exc_type, exc_val, exc_tb)

    def run(self):
        choices = []
        for i, el in enumerate(self.actions):
            choices += [i] * el['prob']
        while True:
            try:
                series = self.get_series()
                self.logger.info(f'START actions serial({series})')
                for act_num in [random.choice(choices) for _ in range(series)]:
                    action = self.actions[act_num]
                    self.logger.info(action['name'].upper())
                    action['exe']()
                sleep_time = fabs(np.random.normal(size=(1, 1))[0, 0] / self.freq)
                self.logger.info(f'END actions serial({series}); sleep=[{sleep_time // 60 // 60}h, {sleep_time // 60 % 60}m, {sleep_time % 60 % 60}s]({sleep_time}s)')
                self.logger.handlers[0].flush()
                sleep(sleep_time)
            except Exception as e:
                self.logger.error(f'{str(e)}')
                self.ssh = Ssh(self.host, self.user, self.psw)