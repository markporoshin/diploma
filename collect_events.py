from kafka import KafkaConsumer
import cefparser
import json
from datetime import datetime


if __name__ == '__main__':
    consumer = KafkaConsumer('Ubuntu_for_SYSCALL', bootstrap_servers='10.0.0.13:30992')
    with open('resource/cef_msg_sysadmin.txt', 'a+') as txt_file:
        with open('resource/cef_event_sysadmin.csv', 'a+') as csv_file:
            csv_file.write(",syscall,uid,dproc,tty,suid,auid,timestamp,ses,date\n")
            for i, msg in enumerate(consumer):
                cef_dict = cefparser.parse(msg.value.decode('utf-8'))

                cef_cols = ['dpid', 'uid', 'dproc', 'terminal/tty', 'suid', 'auid', 'rt', 'ses']
                for col in cef_cols:
                    if col not in cef_dict:
                        cef_dict[col] = 'none'
                row = {
                    'id': i,
                    'syscall': cef_dict['dpid'],
                    'uid': cef_dict['uid'],
                    'dproc': cef_dict['dproc'],
                    'terminal': cef_dict['terminal/tty'],
                    'suid': cef_dict['suid'],
                    'auid': cef_dict['auid'],
                    'timestamp': cef_dict['rt'],
                    'ses': cef_dict['ses']
                }
                date = datetime.fromtimestamp(int(cef_dict['rt']) / 1000)
                cef_dict['date'] = str(date)
                csv_file.write(
                    f"{row['id']},{row['syscall']},{row['uid']},{row['dproc']},{row['terminal']},{row['suid']},{row['auid']},{row['timestamp']},{row['ses']},{date}\n"
                )
                txt_file.write(json.dumps(cef_dict))
                print(json.dumps(cef_dict))
                print(row)
                if i % 1000 == 0:
                    txt_file.flush()
                    csv_file.flush()


